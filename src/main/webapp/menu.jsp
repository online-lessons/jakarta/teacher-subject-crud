<%--
  Created by IntelliJ IDEA.
  User: farkh
  Date: 10/01/2024
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Menu</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <style>
        .menu {
            overflow: hidden;
            position: relative;
            height: 40px;
            transition: .4s;
        }

        .menu-items {
            position: relative;
        }

        a {
            border: 1px solid green;
            height: 40px;
        }

        i {
            height: 40px;
        }
    </style>
</head>
<body>
<div class="menu border w-100 bg-dark text-primary row overflow-hidden">
    <div class="menu-items row border">
        <i class="bi bi-list"></i>
        <a href="http://localhost:8080">Home</a>
        <a href="http://localhost:8080/subject">Subject</a>
        <a href="http://localhost:8080/teacher">Teacher</a>
    </div>
</div>

<script>
    let menu = document.querySelector('.menu')
    menu.addEventListener('click', () => {
        if(menu.style.height === '160px')
            menu.style.height = '40px'
        else menu.style.height = '160px'
    })
</script>
</body>
</html>