package org.example.l7task1teachersubject.dao;

import jakarta.persistence.*;
import lombok.extern.java.Log;
import org.example.l7task1teachersubject.model.Teacher;

import java.util.Collections;
import java.util.List;

@Log
public class TeacherDao implements AutoCloseable {

    private TeacherDao() {
    }

    private static final TeacherDao teacherDao = new TeacherDao();
    public static TeacherDao getInstance() {
        return teacherDao;
    }


    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("teacher-subject");
    private final EntityManager em = entityManagerFactory.createEntityManager();

    public Teacher save(Teacher teacher) {
        begin();
        try {
            em.persist(teacher);
        } catch (Exception e) {
            log.warning(e.getMessage());
        }
        commit();
        return teacher;
    } // ok

    public Teacher getById(Integer id) {
        begin();
        try {
            TypedQuery<Teacher> query = em.createQuery("from Teacher where id = " + id, Teacher.class);
            Teacher teacher = query.getSingleResult();
            commit();
            return teacher;
        } catch (Exception e) {
            log.warning(e.getMessage());
            commit();
            return null;
        }
    } // ok

    public List<Teacher> getAllTeachers() {
        begin();
        try {
            TypedQuery<Teacher> query = em.createQuery("from Teacher ", Teacher.class);
            List<Teacher> resultList = query.getResultList();
            commit();
            return resultList;
        } catch (Exception e) {
            log.warning(e.getMessage());
            commit();
            return Collections.emptyList();
        }
    } // ok

    public Teacher edit(Integer id, Teacher teacher) {
        begin();
        try {
            TypedQuery<Teacher> query = em.createQuery("from Teacher where id = " + id, Teacher.class);
            Teacher editingTeacher = query.getSingleResult();

            editingTeacher.setName(teacher.getName());
            editingTeacher.setGender(teacher.getGender());
            editingTeacher.setSalary(teacher.getSalary());
            editingTeacher.setSubjects(teacher.getSubjects());

            em.merge(editingTeacher);
            commit();
            return editingTeacher;
        }  catch (Exception e) {
            log.warning(e.getMessage());
            commit();
            return null;
        }
    } // ok

    public void delete(Integer id) {
        begin();
        try {
            Query query = em.createQuery("delete from Teacher s where s.id = " + id);
            query.executeUpdate();
            commit();
        } catch (Exception e) {
            log.warning(e.getMessage());
            commit();
        }
    } // ok
    private void begin() {
        em.getTransaction().begin();
    }

    private void commit() {
        em.getTransaction().commit();
    }

    @Override
    public void close() throws Exception {
        em.close();
        entityManagerFactory.close();
    }
}
