package org.example.l7task1teachersubject.controller.subject;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.java.Log;
import org.example.l7task1teachersubject.dao.SubjectDao;
import org.example.l7task1teachersubject.model.Subject;

import java.io.IOException;

@WebServlet("/subject/edit")
@Log
public class SubjectEditServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.valueOf(req.getParameter("id"));
        String name = req.getParameter("name");
        SubjectDao instance = SubjectDao.getInstance();
        Subject edit = instance.edit(id, new Subject(name));
        log.info(edit.toString());
        resp.sendRedirect("/subject");
    }
}
