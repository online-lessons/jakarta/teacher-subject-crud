package org.example.l7task1teachersubject.controller.teacher;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.java.Log;
import org.example.l7task1teachersubject.dao.SubjectDao;
import org.example.l7task1teachersubject.dao.TeacherDao;
import org.example.l7task1teachersubject.enums.Gender;
import org.example.l7task1teachersubject.model.Subject;
import org.example.l7task1teachersubject.model.Teacher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

@WebServlet("/teacher")
@Log
public class TeacherServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        TeacherDao instance = TeacherDao.getInstance();
        req.setAttribute("teachers", instance.getAllTeachers());

        SubjectDao subjectDao = SubjectDao.getInstance();
        req.setAttribute("subjects", subjectDao.getAllSubjects());

        req.getRequestDispatcher("teacher.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        Gender gender = Gender.valueOf(req.getParameter("gender"));
        double salary = Double.parseDouble(req.getParameter("salary"));

        Teacher teacher = new Teacher(name, gender, salary, new ArrayList<>());

        SubjectDao subjectDao = SubjectDao.getInstance();
        String[] ids = req.getParameterValues("subjects");
        for (String id : ids) {
            Subject subject = subjectDao.getById(Integer.valueOf(id));
            teacher.getSubjects().add(subject);
        }

        TeacherDao instance = TeacherDao.getInstance();
        Teacher save = instance.save(teacher);
        log.info(save.toString());
        resp.sendRedirect("/teacher");
    }
}
