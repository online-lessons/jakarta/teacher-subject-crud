package org.example.l7task1teachersubject.controller.subject;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.java.Log;
import org.example.l7task1teachersubject.dao.SubjectDao;
import org.example.l7task1teachersubject.model.Subject;

import java.io.IOException;

@WebServlet("/subject")
@Log
public class SubjectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SubjectDao instance = SubjectDao.getInstance();
        req.setAttribute("subjects", instance.getAllSubjects());
        req.getRequestDispatcher("subject.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        Subject subject = new Subject(name);
        SubjectDao instance = SubjectDao.getInstance();
        Subject save = instance.save(subject);
        log.info(save.toString());
        resp.sendRedirect("/subject");
    }
}
