package org.example.l7task1teachersubject.controller.subject;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.l7task1teachersubject.dao.SubjectDao;

import java.io.IOException;

@WebServlet("/subject/delete")
public class SubjectDeleteServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.valueOf(req.getParameter("id"));
        SubjectDao subjectDao = SubjectDao.getInstance();
        subjectDao.delete(id);
        resp.sendRedirect("/subject");
    }
}
