package org.example.l7task1teachersubject.dao;

import jakarta.persistence.*;
import lombok.extern.java.Log;
import org.example.l7task1teachersubject.model.Subject;

import java.util.Collections;
import java.util.List;

@Log
public class SubjectDao {
    private SubjectDao() {
    }

    private static final SubjectDao subjectDao = new SubjectDao();
    public static SubjectDao getInstance() {
        return subjectDao;
    }

    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("teacher-subject");
    private final EntityManager em = entityManagerFactory.createEntityManager();

    public Subject save(Subject subject) {
        begin();
        try {
            em.persist(subject);
        } catch (Exception e) {
            log.warning(e.getMessage());
        }
        commit();
        return subject;
    }

    public Subject getById(Integer id) {
        begin();
        try {
            TypedQuery<Subject> query = em.createQuery("from Subject where id = " + id, Subject.class);
            Subject subject = query.getSingleResult();
            commit();
            return subject;
        } catch (Exception e) {
            log.warning(e.getMessage());
            commit();
            return null;
        }
    }

    public List<Subject> getAllSubjects() {
        begin();
        try {
            TypedQuery<Subject> query = em.createQuery("from Subject ", Subject.class);
            List<Subject> resultList = query.getResultList();
            commit();
            return resultList;
        } catch (Exception e) {
            log.warning(e.getMessage());
            commit();
            return Collections.emptyList();
        }
    }

    public Subject edit(Integer id, Subject subject) {
        begin();
        try {
            TypedQuery<Subject> query = em.createQuery("from Subject where id = " + id, Subject.class);
            Subject editingSubject = query.getSingleResult();
            editingSubject.setName(subject.getName());
            em.merge(editingSubject);
            commit();
            return editingSubject;
        }  catch (Exception e) {
            log.warning(e.getMessage());
            commit();
            return null;
        }
    }

    public void delete(Integer id) {
        begin();
        try {
            Query query = em.createQuery("delete from Subject s where s.id = " + id);
            query.executeUpdate();
            commit();
        } catch (Exception e) {
            log.warning(e.getMessage());
            commit();
        }
    }
    private void begin() {
        em.getTransaction().begin();
    }

    private void commit() {
        em.getTransaction().commit();
    }

}
