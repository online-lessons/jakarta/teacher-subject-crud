<%--
  Created by IntelliJ IDEA.
  User: farkh
  Date: 10/01/2024
  Time: 15:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE>
<html lang="eng">
<head>
    <title>Subjects</title>
    <style>
        .hide {
            display: none;
        }

        .show {
            display: block;
        }
    </style>
</head>
<body>
<jsp:include page="menu.jsp"/>

<jsp:useBean id="subjects"
             scope="request"
             type="java.util.List<org.example.l7task1teachersubject.model.Subject>"/>

<button class="btn btn-outline-dark add-btn">Add Subject</button>

<div class="my-modal hide" style="
    position: fixed;
    width: 100%;
    height: 100vh;
    background-color: rgba(0, 0, 0, .8);
    /*display: none;*/
">
    <div class="container" style="
        display: flex;
        justify-content: center;
        align-items: center;
    ">
        <form action="http://localhost:8080/subject" method="post">
            <label>
                <input class="form-control" type="text" name="name" placeholder="enter subject name">
            </label>

            <button type="button" class="btn btn-danger cls-btn">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
</div>

<table class="table table-dark table-hover">
    <caption>Subjects</caption>
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="subject" items="${subjects}">
        <tr class="edit-tr">
            <td>${subject.id}</td>
            <td>${subject.name}</td>
            <td class="d-flex justify-content-center align-items-center gap-3">
                <i class="edit-btn bi bi-pen-fill btn btn-outline-warning">
                </i>
                <form method="post"
                      class="m-0 p-0"
                      action="http://localhost:8080/subject/delete?id=${subject.id}">
                    <button class="btn btn-outline-danger">
                        <i class="bi bi-trash3-fill delete-btn"></i>
                    </button>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script>
    let addBtn = document.querySelector('.add-btn')
    let clsBtn = document.querySelector('.cls-btn')
    let modal = document.querySelector('.my-modal')
    let editTrs = document.querySelectorAll('.edit-tr')

    for (let i = 0; i < editTrs.length; i++) {
        let tr = editTrs[i];
        tr.addEventListener('click', (e) => {
            let target = e.target
            console.log(target)
            if (target.classList.contains('edit-btn')) {
                let tds = tr.querySelectorAll('td')
                let id = tds[0].textContent;

                let form = modal.querySelector('form')
                form.action = 'http://localhost:8080/subject/edit?id=' + id;
                let inp = form.querySelector('input')
                inp.value = tds[1].textContent;

                modal.classList.remove('hide')
                modal.classList.add('show')
            }
        })
    }

    addBtn.addEventListener('click', () => {
        let form = modal.querySelector('form')
        form.action = 'http://localhost:8080/subject';
        let inp = form.querySelector('input')
        inp.value = '';

        modal.classList.remove('hide')
        modal.classList.add('show')
    })

    clsBtn.addEventListener('click', () => {
        modal.classList.remove('show')
        modal.classList.add('hide')
    })
</script>

</body>
</html>
