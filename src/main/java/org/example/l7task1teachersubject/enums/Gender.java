package org.example.l7task1teachersubject.enums;

public enum Gender {
    MALE, FEMALE
}
