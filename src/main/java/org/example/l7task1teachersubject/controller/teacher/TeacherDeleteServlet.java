package org.example.l7task1teachersubject.controller.teacher;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.l7task1teachersubject.dao.SubjectDao;
import org.example.l7task1teachersubject.dao.TeacherDao;
import org.example.l7task1teachersubject.model.Teacher;

import java.io.IOException;

@WebServlet("/teacher/delete")
public class TeacherDeleteServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer id = Integer.valueOf(req.getParameter("id"));
        TeacherDao teacherDao = TeacherDao.getInstance();
        teacherDao.delete(id);
        resp.sendRedirect("/teacher");
    }
}
