package org.example.l7task1teachersubject.model;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.example.l7task1teachersubject.enums.Gender;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    Gender gender;

    double salary;

    @ManyToMany
    List<Subject> subjects;

    public Teacher(String name, Gender gender, double salary, List<Subject> subjects) {
        this.name = name;
        this.gender = gender;
        this.salary = salary;
        this.subjects = subjects;
    }
}
